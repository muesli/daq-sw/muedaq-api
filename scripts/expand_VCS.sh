#!/bin/sh

mydate=`git log --pretty=format:"%ad" -1`
git describe --tags >& ./.tmp
if (( `grep -c fatal ./.tmp` > 0 )); then
    mytag="None"
else
    mytag=`git describe --tags`
fi;
rm ./.tmp

sed -e 's,\$Date\$,\$Date '"$mydate"'\$,' | sed -e 's,\$Tag\$,\$Tag '"$mytag"'\$,' 
