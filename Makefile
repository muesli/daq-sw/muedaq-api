#
# muedaq-api Makefile
#
# external variables : LEGACY_FORMAT, EVB, DEBUG
#

CC=gcc
CPP=g++ 
CFLAGS=-std=c99 -Ofast -D_GNU_SOURCE -g 

ifdef LEGACY_FORMAT
	CFLAGS := $(CFLAGS) -DLEGACY_FORMAT
endif
ifdef DEBUG
	CFLAGS := $(CFLAGS) -DDEBUG
endif
ifdef EVB
	CFLAGS := $(CFLAGS) -DEVB
endif

INC_DIR = include/ ../include
SRC_DIR = src/
OBJ_DIR = obj/
LIB_DIR = lib/

API_SRCS = muedaq_version.c \
	muedaq_file.c \
	muedaq_metadata.c \
	muedaq_link.c \
	muedaq_payload.c \
	muedaq_evtbuilder.c
API_SRCS := $(addprefix $(SRC_DIR),$(API_SRCS))
API_OBJS = $(patsubst $(SRC_DIR)%.c, $(OBJ_DIR)%.o, $(API_SRCS))


#all_targets = lib $(shell [ "x$${JUMBO_LINK}" != "x1" ] && echo test)
#all_targets := $(all_targets) $(shell [ "x$${JUMBO_LINK}" == "x1" ] && echo jumbo_decode)
#all : $(all_targets)

all : lib test 


test :  bin/decode_with_api_c bin/decode_with_api_cc bin/decode_builder_with_api
lib : $(LIB_DIR)/libmuedaq.a


obj/%.o : src/%.c
	$(CC) $(CFLAGS) $(addprefix -I, $(INC_DIR)) -c $< -o $@ 

$(LIB_DIR)/libmuedaq.a : $(API_OBJS)
	$(AR) -rfv $(LIB_DIR)/libmuedaq.a $^


# no wildcards because of .c vs. cc
bin/decode_with_api_c : src/decode_with_api.c lib
	$(CC) $(CFLAGS) $(addprefix -I, $(INC_DIR)) $< -o ./bin/decode_with_api_c -L$(LIB_DIR) -lmuedaq
bin/decode_with_api_cc : src/decode_with_api.cc lib
	$(CPP) -O2 $(addprefix -I, $(INC_DIR)) $< -o ./bin/decode_with_api_cc -L$(LIB_DIR) -lmuedaq
bin/test_raw : src/test_raw.c  
	$(CC) $(CFLAGS) $(addprefix -I, $(INC_DIR)) $< -o ./bin/test_raw  -L$(LIB_DIR) -lmuedaq
bin/test_file_output : src/test_file_output.c  
	$(CC) $(CFLAGS) $(addprefix -I, $(INC_DIR)) $< -o ./bin/test_file_output  -L$(LIB_DIR) -lmuedaq
bin/decode_builder_with_api : src/decode_builder_with_api.c lib
	$(CC) $(CFLAGS) $(addprefix -I, $(INC_DIR)) $< -o ./bin/decode_builder_with_api_c -L$(LIB_DIR) -lmuedaq


depend : .depend
.depend : $(API_SRCS)
	- rm -f "$@"
	$(CC) $(CFLAGS) $(addprefix -I, $(INC_DIR)) -MM $^ > "$@"
	- sed -ie "s,\(.*\).o:,obj/\1.o:," "$@"
include .depend

clean : 
	rm -f src/*~
	rm -f include/*~
	rm -f bin/*
	rm -f obj/*
	rm -f lib/*
	rm .depend


