#include <stdio.h>
#include <stdint.h>
#include "muedaq_version.h"

//extern const char * api_id;
//extern const char * api_tag;
//extern const char * api_date;

//----------------------------------------------------------
void
muedaq_print_version()
//----------------------------------------------------------
{
  printf( " --- muedaq API version ---\n");
  printf( "%s\n", api_id);
  printf( "%s\n", api_tag);
  printf( "%s\n", api_date);  
  printf("\n");
}

//----------------------------------------------------------
const char * 
muedaq_api_get_tag()
//----------------------------------------------------------
{
  return api_tag;
}

//----------------------------------------------------------
void
muedaq_get_api_version(const char * vstring,
			uint64_t* api_major,
			uint64_t* api_minor,
			uint64_t* api_patch )
//----------------------------------------------------------
{
  sscanf( vstring, "$Tag v%d.%d.%d%*",
	  &api_major,&api_minor,&api_patch );
}
