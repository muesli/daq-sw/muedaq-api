#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <getopt.h>

#include "muedaq_api.h"


//-----------------------------------------------------------------------
void
print_metadata_format()
//-----------------------------------------------------------------------
{
  printf("\n\nmetadata format:\n");
  printf("0: R R R S | S S S S | S S S S | E E E E\n");
  printf("1: E E E E | E T T T | T T T T | T T T T(3-1)X(0)\n");
}

//-----------------------------------------------------------------------
void
print_link_header_format()
//-----------------------------------------------------------------------
{
  printf("\n\nlink header format:\n");
  printf("0: C C L L | R R R S | S S S S | S S S S\n");
}

//-----------------------------------------------------------------------
void
print_payload_header_format()
//-----------------------------------------------------------------------
{
  printf("\n\npayload header format:\n");
  printf("0: U U             U  U |  U  U  U  U |  O  O  O             O  |  O  O  O  O\n");
  printf("1: C C R(3-2)S2(0-1) S2 | S2 S2 S2 S1 | S1 S1 S1 S1(3-2)S0(1-0) | S0 S0 S0 S0\n");
  printf("2: R R             R  R |  R  R  R  R |  R  R  R  R(3-2)S3(1-0) | S3 S3 S3 S3\n");  
}


//-----------------------------------------------------------------------
void
print_payload_stub_format()
//-----------------------------------------------------------------------
{
  printf("\n\npayload stub format:\n");
  printf("0: LN  CI(3)BX(2-1) BX  BX | BX(3)CB(2-0) AD AD BN\n");
}


//-----------------------------------------------------------------------
int
main(int argc, char** argv)
//-----------------------------------------------------------------------
{



  //muedaq_print_version();

  
  /************* metadata  *****************/
  MetaData  m;
  m.start_seq = 0xe;
  m.end_seq   = 0xf;
  m.run       = 0x123;
  m.api_major = 0x1;
  m.api_minor = 0x0;
  m.api_patch = 0x0;

  //muedaq_print_metadata(&m);  

  uint64_t * meta_ptr = (uint64_t*)&m;




  //
  // below we'll make a frame with 36 empty packets and 1 packet that contains 3 stubs 
  //
  #define N_PAYLOAD_PACKETS 37
  #define N_STUBS 3

  // 3x 64b header + 1 64b delimiter for every packet.  No delimiter for the last packet
  unsigned frame_length = N_PAYLOAD_PACKETS * ( sizeof(PayloadHeader)/8 + 1 ) - 1 ; 
  // one of those packets will have 3 32b stubs, which should be padded to  64b
  frame_length += ((N_STUBS%2) + (N_STUBS/2));



  /************* link header  *****************/
  LinkHeader l;
  l.sequence = 0xe;
  l.run      = 0x123;
  l.length   = frame_length; // in 64b words 
  l.code     = 0xac;  // aggregated 

  //muedaq_print_link_header(&l);

  uint64_t * lhdr_ptr = (uint64_t*)&l;

  
  /************* payload header  *****************/
  PayloadHeader phdr[N_PAYLOAD_PACKETS];
  for( int i=0; i<N_PAYLOAD_PACKETS; i++ ) { 
    phdr[i].bx_super_ID      = 0x12345678;
    phdr[i].user_data        = 0xdeadbeef;
    phdr[i].bx_ID            = (unsigned)i; // increment bxID 
    phdr[i].stub_count       = 0; // start off with zero stubs in all packets
    phdr[i].module_0_status  = 0x11;
    phdr[i].module_1_status  = 0x22;
    phdr[i].module_2_status  = 0x33;
    phdr[i].module_3_status  = 0x44;
  }

  // now give stubs to one of the packets for variety's sake
  phdr[11].stub_count = N_STUBS;

  // stubs for that packet
  PayloadStub pstub[N_STUBS];

  // give them random values 
  pstub[0].z         = 0xa;
  pstub[0].bend      = 0xb;
  pstub[0].addr      = 0xcd;
  pstub[0].cbc_ID    = 0x2;
  pstub[0].bx_offset = 0x7;
  pstub[0].cic_ID    = 0x1;
  pstub[0].FE_type   = 0x0;
  pstub[0].link_ID   = 0xe;

  pstub[1].z         = 0x0;
  pstub[1].bend      = 0x1;
  pstub[1].addr      = 0x23;
  pstub[1].cbc_ID    = 0x0;
  pstub[1].bx_offset = 0x1;
  pstub[1].cic_ID    = 0x0;
  pstub[1].FE_type   = 0x1;
  pstub[1].link_ID   = 0xd;

  pstub[2].z         = 0xf;
  pstub[2].bend      = 0xe;
  pstub[2].addr      = 0x78;
  pstub[2].cbc_ID    = 0x3;
  pstub[2].bx_offset = 0x3;
  pstub[2].cic_ID    = 0x1;
  pstub[2].FE_type   = 0x1;
  pstub[2].link_ID   = 0xb;


  // pointer to the stub data
  uint32_t * pstub_ptr = (uint32_t*)&(pstub[0]);

  // an empty stub for padding
  uint32_t  null_stub      = 0x0;
  uint32_t* null_stub_ptr  = &null_stub;

  // and the separator between packets
  uint64_t delimiter = PACKET_DELIMITER;
  uint64_t* delimiter_ptr = &delimiter;


  /************* now output  *****************/
  FILE * fh_raw_out = fopen ("./test_raw.dat", "wb");
  fwrite(meta_ptr, sizeof(MetaData), 1, fh_raw_out);
  fwrite(lhdr_ptr, sizeof(LinkHeader), 1, fh_raw_out);
  for( int i=0; i<N_PAYLOAD_PACKETS; i++ ) {
    uint64_t* phdr_ptr = (uint64_t*)&(phdr[i]);
    fwrite(phdr_ptr, sizeof(PayloadHeader), 1, fh_raw_out);
    if( i == 11 ) { 
      for( int j=0; j<3; j++ )  
	fwrite(pstub_ptr+j, sizeof(PayloadStub), 1, fh_raw_out);
      // pad
      fwrite(null_stub_ptr, sizeof(PayloadStub), 1, fh_raw_out);
    }
    fwrite(delimiter_ptr, sizeof(delimiter), 1, fh_raw_out);
  }

  fclose(fh_raw_out);
  
}

