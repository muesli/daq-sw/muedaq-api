#include <stdio.h>
#include "muedaq_metadata.h"


#ifndef LEGACY_FORMAT

//----------------------------------------------------------
void
muedaq_encode_metadata(MetaData *ptr,
		       uint64_t run,
		       uint64_t start_seq,
		       uint64_t end_seq,
		       uint64_t seqerr,
		       uint64_t api_major,
		       uint64_t api_minor,
		       uint64_t api_patch )
//----------------------------------------------------------
{
  ptr->run       = run;
  ptr->start_seq = start_seq;
  ptr->end_seq   = end_seq;
  ptr->seqerr    = seqerr;
  ptr->api_major = api_major;
  ptr->api_minor = api_minor;
  ptr->api_patch = api_patch;  
  return;
}


//----------------------------------------------------------
void
muedaq_decode_metadata(MetaData *ptr,
		       uint64_t *run,
		       uint64_t *start_seq,
		       uint64_t *end_seq,
		       uint64_t *seqerr,
		       uint64_t *api_major,
		       uint64_t *api_minor,
		       uint64_t *api_patch)		       
//----------------------------------------------------------
{
  *run       = ptr->run;
  *start_seq = ptr->start_seq;
  *end_seq   = ptr->end_seq;
  *seqerr    = ptr->seqerr;
  *api_major = ptr->api_major;
  *api_minor = ptr->api_minor;
  *api_patch = ptr->api_patch;  
  return;
}  


//----------------------------------------------------------
void
muedaq_print_metadata(MetaData* metadata)
//----------------------------------------------------------
{
  uint64_t run,seqerr,start_seq,end_seq,api_major,api_minor,api_patch;
  muedaq_decode_metadata(metadata,&run,&start_seq,&end_seq,&seqerr,
			 &api_major,&api_minor,&api_patch);
  printf("======= file metadata =======\n");
  printf( "file metadata | run            : 0x%03x\n", run);
  printf( "file metadata | start sequence : 0x%09llx\n", start_seq);
  printf( "file metadata | end sequence   : 0x%09llx\n", end_seq);
  printf( "file metadata | sequence error : 0x%1x\n", seqerr);      	      
  printf( "file metadata | API.major      : 0x%02d\n", api_major);
  printf( "file metadata | API.minor      : 0x%02d\n", api_minor);
  printf( "file metadata | API.patch      : 0x%02d\n", api_patch);      
  printf("\n");
  return;
}

uint64_t muedaq_metadata_get_end_seq(MetaData * mptr)
{
  return mptr->end_seq;
}

uint64_t muedaq_metadata_get_api_major(MetaData * mptr)
{
  return mptr->api_major;
}

uint64_t muedaq_metadata_get_api_minor(MetaData * mptr)
{
  return mptr->api_minor;
}

uint64_t muedaq_metadata_get_api_patch(MetaData * mptr)
{
  return mptr->api_patch;
}

void muedaq_metadata_set_api_version(MetaData * mptr, const char* vstring)
{
  uint64_t api_major,api_minor,api_patch;
  sscanf( vstring, "$Tag v%d.%d.%d%*",
	  &api_major,&api_minor,&api_patch );
  mptr->api_major = api_major;
  mptr->api_minor = api_minor;
  mptr->api_patch = api_patch;  
}



#else




//----------------------------------------------------------
void
muedaq_encode_metadata(MetaData *ptr,
		uint64_t run,
		uint64_t start_seq,
		uint64_t end_seq,
		uint64_t seqerr,
		uint64_t tstamp)
//----------------------------------------------------------
{
  ptr->run       = run;
  ptr->start_seq = start_seq;
  ptr->end_seq_1 = (end_seq&0xFFFF);
  ptr->end_seq_2 = ((end_seq>>16)&0xFFFFF)<<44;
  ptr->seqerr    = seqerr;
  ptr->tstamp    = tstamp;
  return;
}


//----------------------------------------------------------
void
muedaq_decode_metadata(MetaData *ptr,
		       uint64_t *run,
		       uint64_t *start_seq,
		       uint64_t *end_seq,
		       uint64_t *seqerr,
		       uint64_t *tstamp)
//----------------------------------------------------------
{
  *run       = ptr->run;
  *start_seq = ptr->start_seq;
  uint64_t high = ptr->end_seq_1;
  uint64_t low  = ptr->end_seq_2;
  *end_seq   = (high<<20UL) |  low;
  *seqerr    = ptr->seqerr;
  *tstamp    = ptr->tstamp;
  return;
}  


//----------------------------------------------------------
void
muedaq_print_metadata(MetaData* metadata)
//----------------------------------------------------------
{
  uint64_t run,seqerr,start_seq,end_seq,tstamp;
  muedaq_decode_metadata(metadata,&run,&start_seq,&end_seq,&seqerr,&tstamp);
  printf("======= file metadata =======\n");
  printf( "file metadata | run            : 0x%03x\n", run);
  printf( "file metadata | start sequence : 0x%09llx\n", start_seq);
  printf( "file metadata | end sequence   : 0x%09llx\n", end_seq);
  printf( "file metadata | sequence error : 0x%1x\n", seqerr);      	      
  printf( "file metadata | timestamp      : 0x%011llx\n", tstamp);    
  printf("\n");
  return;
}


uint64_t muedaq_metadata_get_end_seq(MetaData * mptr)
{
  uint64_t high = mptr->end_seq_1;
  uint64_t low  = mptr->end_seq_2;
  return (high<<20UL) |  low;
}

uint64_t muedaq_metadata_get_tstamp(MetaData * mptr)
{
  return mptr->tstamp;
}



#endif 





//----------------------------------------------------------
uint64_t*
muedaq_metadata_to_raw(MetaData *ptr)
//----------------------------------------------------------
{
  return (uint64_t*)ptr;
}


//----------------------------------------------------------
MetaData*
muedaq_raw_to_metadata(uint64_t *ptr)

//----------------------------------------------------------
{
  return (MetaData*)ptr;
}



//
// accessors
//

uint64_t muedaq_metadata_get_run(MetaData * mptr)
{
  return mptr->run;
}

uint64_t muedaq_metadata_get_start_seq(MetaData * mptr)
{
  return mptr->start_seq;
}

uint64_t muedaq_metadata_get_seqerr(MetaData * mptr)
{
  return mptr->seqerr;
}

