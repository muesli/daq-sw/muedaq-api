#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <getopt.h>

#include "muedaq_api.h"

//-----------------------------------------------------------------------
void
print_metadata_format()
//-----------------------------------------------------------------------
{
  printf("\n\nmetadata format:\n");
  printf("0: R R R S | S S S S | S S S S | E E E E\n");
  printf("1: E E E E | E T T T | T T T T | T T T T(3-1)X(0)\n");
}

//-----------------------------------------------------------------------
void
print_link_header_format()
//-----------------------------------------------------------------------
{
  printf("\n\nlink header format:\n");
  printf("0: C C L L | R R R S | S S S S | S S S S\n");
}

//-----------------------------------------------------------------------
void
print_payload_header_format()
//-----------------------------------------------------------------------
{
  printf("\n\npayload header format:\n");
  printf("0: U U             U  U |  U  U  U  U |  O  O  O             O  |  O  O  O  O\n");
  printf("1: C C R(3-2)S2(0-1) S2 | S2 S2 S2 S1 | S1 S1 S1 S1(3-2)S0(1-0) | S0 S0 S0 S0\n");
  printf("2: R R             R  R |  R  R  R  R |  R  R  R  R(3-2)S3(1-0) | S3 S3 S3 S3\n");  
}


//-----------------------------------------------------------------------
void
print_payload_stub_format()
//-----------------------------------------------------------------------
{
  printf("\n\npayload stub format:\n");
  printf("0: LN  CI(3)BX(2-1) BX  BX | BX(3)CB(2-0) AD AD BN\n");
}


//-----------------------------------------------------------------------
int
main(int argc, char** argv)
//-----------------------------------------------------------------------
{

  muedaq_print_version();

  
  /************* metadata serialization test *****************/
  printf(" ************* metadata serialization test ***************** \n");
  char mbuf[16] = { 0xef, 0xcd, 0xab, 0x89, 0x67, 0x45, 0x23, 0x01,
		    0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0x99, 0x11  } ;
  
  printf("\nraw as hex: \n" );
  for( int i=0; i<2; i++ ) {
    for( int j=0; j<8; j++ ) {    
      printf("%02lx ", mbuf[i*8+j]&0xff );
    }
    printf("\n");
  }
  printf("\nraw as u64: \n" );
  uint64_t * mptr = (uint64_t*)mbuf;
  for( int i=0; i<2; i++ ) {  
    printf("[%d] :  0x%016llx\n", i, *(mptr+i));    
  }

  print_metadata_format();
  
  printf("\ncoverted to MetaData:\n");  
  MetaData * m = muedaq_raw_to_metadata((uint64_t*)mbuf);
  muedaq_print_metadata(m);

  uint64_t * mraw =  muedaq_metadata_to_raw(m);
  printf("back to u64: \n" );
  for( int i=0; i<2; i++ ) {  
    printf("[%d] :  0x%016llx\n", i, *(mraw+i));    
  }
  printf("\n");


  /************* link header serialization test *****************/
  printf(" ************* link header serialization test ***************** \n");
  char lbuf[8] = { 0xef, 0xcd, 0xab, 0x89, 0x67, 0x45, 0x23, 0x01 };
  printf("\nraw as hex: \n" );
  for( int i=0; i<8; i++ ) {    
    printf("%02lx ", lbuf[i]&0xff );
  }
  printf("\n");
  printf("\nraw as u64: \n" );
  uint64_t * lptr = (uint64_t*)lbuf;
  printf("[0] :  0x%016llx\n",  *(lptr));    

  print_link_header_format();
  
  printf("\ncoverted to LinkHeader:\n");
  LinkHeader * lhdr = muedaq_raw_to_link_header(lptr);
  muedaq_print_link_header(lhdr);

  uint64_t lraw = muedaq_link_header_to_raw(lhdr);
  printf("\nback to u64: \n" );
  printf( "[0] : 0x%016llx\n", lraw );    
  printf("\n");
  
  /************* payload header serialization test *****************/
  printf(" ************* payload header serialization test ***************** \n");
  char pbuf[24] = { 0xef, 0xcd, 0xab, 0x89, 0x67, 0x45, 0x23, 0x01,
		    0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0x99, 0x11,
		    0x0f, 0x1e, 0x2d, 0x3c, 0x4b, 0x5a, 0x69, 0x78} ;

  printf("\nraw as hex: \n" );
  for( int i=0; i<3; i++ ) {
    for( int j=0; j<8; j++ ) {    
      printf("%02lx ", pbuf[i*8+j]&0xff );
    }
    printf("\n");
  }
  printf("\nraw as u64: \n" );
  uint64_t * pptr = (uint64_t*)pbuf;
  for( int i=0; i<3; i++ ) {  
    printf("[%d] :  0x%016llx\n", i, *(pptr+i));    
  }
  
  print_payload_header_format();

  printf("\ncoverted to PayloadHeader:\n");  
  PayloadHeader * phdr = muedaq_raw_to_payload_header(pptr);
  muedaq_print_payload_header(phdr);
  printf("\n");

  uint64_t * praw = muedaq_payload_header_to_raw(phdr);
  printf("back to u64: \n" );
  for( int i=0; i<3; i++ ) {  
    printf("[%d] :  0x%016llx\n", i, *(praw+i));    
  }
  printf("\n");



  /************* payload stub serialization test *****************/
  printf(" ************* payload stub serialization test ***************** \n");
  char sbuf[4] = {  0xef, 0xcd, 0xab, 0x89 } ;

  printf("\nraw as hex: \n" );
  for( int i=0; i<4; i++ ) {
    printf("%02lx ", sbuf[i]&0xff );
  }
  printf("\n");
  printf("\nraw as u32: \n" );
  unsigned * sptr = (unsigned*)sbuf;
  printf("[0] :  0x%08x\n",  *(sptr));    

  print_payload_stub_format();

  printf("\ncoverted to PayloadStub:\n");    
  PayloadStub * pstub = muedaq_raw_to_payload_stub(sptr);
  muedaq_print_payload_stub(pstub);
  printf("\n");

  unsigned  sraw = muedaq_payload_stub_to_raw(pstub);
  printf("back to u32: \n" );
  printf("[0] :  0x%08x\n", sraw);    
  printf("\n");	 

  
}

