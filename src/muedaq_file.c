#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

#include "muedaq_file.h"



//
// exposed by the API
//



//----------------------------------------------------------
void
muedaq_open_file(const char * filename,
		 DAQFile * data )
//----------------------------------------------------------
{
  data->fh = fopen(filename, "rb");
  sprintf(data->filename, "%s",filename);
  fread(&(data->metadata),METADATA_WORDS,8,data->fh); // 8x1B = 64b
}

//----------------------------------------------------------
void
muedaq_close_file( DAQFile * data )
//----------------------------------------------------------
{
  if( data->fh != NULL )  fclose(data->fh);
  memset(&(data->metadata),0,sizeof(MetaData));
}

