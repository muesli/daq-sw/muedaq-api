#include <stdio.h>
#include "muedaq_link.h"
#include "muedaq_evtbuilder.h"


//----------------------------------------------------------
// Internal, but for offline use in reading in data produced
// by the evtbuilder.  This routine is similar to the link
// case : we drop pointers to the loaded buffer into the
// packet array.  There should be no empty packets output
// from the evtbuilder, and n_packet sohuld always =1 but 
// we'll keep the checks ...
//----------------------------------------------------------
int
assign_builder_frame_packet_ptrs( BuilderFrame* frame)
//----------------------------------------------------------
{
  const int kPayloadHeaderSize_w64 = sizeof(PayloadHeader)/sizeof(uint64_t);

  // want a copy of the buffer pointer, internal_load does the actual increment
  uint64_t* buffer     = frame->buffer;
  uint64_t* buffer_ptr = frame->buffer_ptr;  
  
  #ifdef DEBUG
  printf("assign_builder_frame_packet_ptrs : loading %d uint64, frame=0x%llx, packet=0x%llx\n",
  	 frame->header.length,frame,&(frame->packet));
  #endif

  int wcnt=0, pcnt=0;
  while( wcnt < frame->header.length-1  ) 
    {
      //store the packet header
      frame->packet_ptr->header_ptr = (PayloadHeader*)(buffer_ptr);
      buffer_ptr += kPayloadHeaderSize_w64;    
      wcnt += kPayloadHeaderSize_w64;
      
#ifdef DEBUG
      printf("assign_builder_frame_packet_ptrs : packet header superID=0x%llx\n",
	     frame->packet_ptr->header_ptr->bx_super_ID );
#endif

      int scnt=0,wcnt32=0;
      uint32_t* u32p = (uint32_t*)(buffer_ptr);
      frame->packet_ptr->stub_ptr = (PayloadStub*)(u32p);
      // intermediate++, see below
      uint32_t* u32p_orig = u32p;
      uint32_t* u32p_last_nonzero = u32p;      
      
      // stub data loop, no -1 so as to reach the delimiter
      while( *u32p != (PACKET_DELIMITER&0xFFFFFFFFU) && wcnt < frame->header.length  ) 
	{ 
	  if( *u32p != 0 ) { 
	    
	    // old case : add stub by copying
	    //pktarr_ptr->stub_ptr[scnt] = (PayloadStub*)(u32p);
	    
	    // intermediate case : just count non-zero stubs, copy can miss stubs
	    // if zero-stubs are in between
	    scnt++;
	    
	    //intermediate++ : go until the last non-zero stub.  everything in between will be copied
	    // see below ...
	    u32p_last_nonzero = u32p;
	} 
	  
	  wcnt += wcnt32%2;         // advance in buffer
	  (buffer_ptr) += wcnt32%2; 
	  wcnt32++;
	  u32p++;                  // advance local stub ptr
	}

      // for jumbo frame, store only packets with stubs
      if( scnt > 0 ) { 
	// intermediate++
	scnt = u32p_last_nonzero - u32p_orig +1 ; //override scnt
	frame->packet_ptr->n_stubs = scnt;  // note stub couunt
#ifdef DEBUG
	printf("assign_builder_frame_packet_ptrs : packet %d n_stubs set to %d\n",
	       frame->packet_ptr-frame->packet, scnt);
#endif
	(frame->packet_ptr) += 1; // next packet only if stubs
	pcnt++;
      }
      wcnt++; // always advance in buffer
      (buffer_ptr) += 1 ;
    }
  frame->n_packets += pcnt;
  
  return pcnt;
}



//----------------------------------------------------------
// Internal, but for offline use in reading in data produced
// by the evtbuilder.  This routine is similar to the link
// case : we copy from the file to the internal buffer 
//----------------------------------------------------------
int
internal_load_builder_frame(DAQFile *data, BuilderFrame* frame)
//----------------------------------------------------------
{

  // reset 
  frame->buffer_ptr = &(frame->buffer[0]);
  frame->packet_ptr = &(frame->packet[0]);  
  
  //
  // fill the frame with data at the appropriate offset 
  // while loop is case we need to read ahead ...
  //
  int n_frame = 0, n_packet=0;
  while( !feof(data->fh) ) { 
    
    // read in the builder header
    fread(&frame->header,1,8,data->fh); // 8x1B = 64b
    
    if( !feof(data->fh) && 
	(frame->header.code == HEADER_CODE_BUILT ) ) {     
     
      // fill the payload buffer w/ 64b builder frame data 
      fread(frame->buffer_ptr,frame->header.length,8,data->fh);
      n_frame++;
      
      // drop in the pointers
      n_packet = assign_builder_frame_packet_ptrs( frame );
      
      // check
      if( frame->packet_ptr - frame->packet != n_packet ) {
	printf("frame->packet_ptr  : 0x%llx\n", frame->packet_ptr );
	printf("frame->packet      : 0x%llx\n", frame->packet );
	printf("n_packet           : %d\n", n_packet );
	exit(1);
      }
      
#ifdef DEBUG
      printf("internal_load_builder_frame : %d packet pointers assigned\n", n_packet);          
#endif
      
    } // if valid frame

    if( n_frame ) break; // just want one frame
    
  }

  // update frame's n_packet
  frame->n_packets = n_packet;
  if( !n_packet ) frame->packet_ptr = NULL;
  else frame->packet_ptr = frame->packet;
  return n_packet;
}






//
// routines exposed by the API
//



//----------------------------------------------------------
// As for the LinkFrame, but for the BuilderFrame
//----------------------------------------------------------
bool
muedaq_next_builder_frame(DAQFile * data, BuilderFrame * frame)
//----------------------------------------------------------
{
  int ret = internal_load_builder_frame(data,frame) ;
  if ( 0 < ret ) {
    frame->buffer_ptr = frame->buffer;
    frame->packet_ptr = frame->packet;
    return true;
  }
  frame->buffer_ptr = frame->buffer;
  frame->packet_ptr = NULL;
  return false;
}



//----------------------------------------------------------
void
muedaq_decode_builder_header(BuilderHeader * lhdr,
			   unsigned * pkt_code,
			   unsigned * pkt_len,
			   unsigned * pkt_run)
//----------------------------------------------------------
{
  // with acessors
  *pkt_code = muedaq_builder_get_code(lhdr);
  *pkt_len  = muedaq_builder_get_length(lhdr);
  *pkt_run  = muedaq_builder_get_run(lhdr);
  return;
}


//----------------------------------------------------------
void
muedaq_encode_builder_header(BuilderHeader * lhdr,
			     unsigned  pkt_code,
			     unsigned  pkt_len,
			     unsigned  pkt_run)
//----------------------------------------------------------
{
  lhdr->code     = pkt_code;
  lhdr->length   = pkt_len;  
  lhdr->run      = pkt_run;  
  return;
}


//----------------------------------------------------------
uint64_t
muedaq_builder_header_to_raw(BuilderHeader* lhdr)
//----------------------------------------------------------
{
  return *((uint64_t*)lhdr);
}


//----------------------------------------------------------
BuilderHeader*
muedaq_raw_to_builder_header(uint64_t* raw)
//----------------------------------------------------------
{
  return (BuilderHeader*)raw;
}


//----------------------------------------------------------
void
muedaq_print_builder_header(BuilderHeader* lhdr)
//----------------------------------------------------------
{
  unsigned code,length,run;
  muedaq_decode_builder_header(lhdr,&code,&length,&run);

  char * code_str;
  if( code == HEADER_CODE_NORMAL )         code_str="normal";
  else if( code == HEADER_CODE_COMBINED )  code_str="aggregate";
  else if( code == HEADER_CODE_HEARTBEAT ) code_str="heartbeat";
  else if( code == HEADER_CODE_BUILT )     code_str="evtbuilder";    
  else code_str="invalid";
  
  printf( "header code    : 0x%2x (%s)\n", code,code_str);
  printf( "payload length : 0x%2x\n",      length);      
  printf( "run#           : 0x%3x\n",      run);      
  return; 
}

//
// accessors
//
uint64_t muedaq_builder_get_run(BuilderHeader * lptr)
{
  return lptr->run;
}
uint64_t muedaq_builder_get_length(BuilderHeader * lptr)
{
  return lptr->length;
}
uint64_t muedaq_builder_get_code(BuilderHeader * lptr)
{
  return lptr->code;
}




//
// now, internal routines for the evtbuilder
//


#ifdef EVB

//----------------------------------------------------------
// this is similar to the link case : drops pointers to the
// loaded buffer into the packet array.  Jumbo case throws
// away empty packets.  Last 2 arguments are for the evb 
//----------------------------------------------------------
int
assign_jumbo_frame_packet_ptrs( JumboFrame* frame,
				int ID, int index )
//----------------------------------------------------------
{
  const int kPayloadHeaderSize_w64 = sizeof(PayloadHeader)/sizeof(uint64_t);

  // want a copy of the buffer pointer, internal_load does the actual increment
  uint64_t* buffer     = frame->buffer;
  uint64_t* buffer_ptr = frame->buffer_ptr;  
  
  #ifdef DEBUG
  printf("assign_jumbo_packet_ptrs : loading %d uint64, frame=0x%llx, packet=0x%llx\n",
  	 frame->header.length,frame,&(frame->packet));
  #endif

  // get the length of this *link frame*, which is kept in the jumbo header, 
  // and overwritten on every call.  It is updated with final length of the 
  // full jumbo frame before transmission
  int wcnt=0, pcnt=0;
  while( wcnt < frame->header.length-1  ) // jumbo hdr presently holds length for this link frame ...
    {
      //store the packet header
      frame->packet_ptr->header_ptr = (PayloadHeader*)(buffer_ptr);
      buffer_ptr += kPayloadHeaderSize_w64;    
      wcnt += kPayloadHeaderSize_w64;
      
#ifdef DEBUG
      printf("assign_jumbo_packet_ptrs : packet header superID=0x%llx\n",
	     frame->packet_ptr->header_ptr->bx_super_ID );
#endif

      int scnt=0,wcnt32=0;
      uint32_t* u32p = (uint32_t*)(buffer_ptr);
      frame->packet_ptr->stub_ptr = (PayloadStub*)(u32p);
      // intermediate++, see below
      uint32_t* u32p_orig = u32p;
      uint32_t* u32p_last_nonzero = u32p;      
      
      // stub data loop, no -1 so as to reach the delimiter
      while( *u32p != (PACKET_DELIMITER&0xFFFFFFFFU) && wcnt < frame->header.length  ) 
	{ 
	  if( *u32p != 0 ) { 
	    
	    // old case : add stub by copying
	    //pktarr_ptr->stub_ptr[scnt] = (PayloadStub*)(u32p);
	    
	    // intermediate case : just count non-zero stubs, copy can miss stubs
	    // if zero-stubs are in between
	    scnt++;
	    
	    //intermediate++ : go until the last non-zero stub.  everything in between will be copied
	    // see below ...
	    u32p_last_nonzero = u32p;
	} 
	  
	  wcnt += wcnt32%2;         // advance in buffer
	  (buffer_ptr) += wcnt32%2; 
	  wcnt32++;
	  u32p++;                  // advance local stub ptr
	}

      // for jumbo frame, store only packets with stubs
      if( scnt > 0 ) { 
	// intermediate++
	scnt = u32p_last_nonzero - u32p_orig +1 ; //override scnt
	frame->packet_ptr->n_stubs = scnt;  // note stub couunt
#ifdef DEBUG
	printf("assign_packet_ptrs : packet %d n_stubs set to %d\n",
	       frame->packet_ptr-frame->packet, scnt);
#endif
	(frame->packet_ptr) += 1; // next packet only if stubs
	pcnt++;
      }
      wcnt++; // always advance in buffer
      (buffer_ptr) += 1 ;
    }
  frame->n_packets += pcnt;
  frame->fillcount += 1;
  
  return pcnt;
}



//----------------------------------------------------------
// note: invoked by the evb frame filling threads.  Last 2
// arguments are for that purpose
//
// This reads all of the link frames in a file into a big
// buffer.  After each link frame is read, packet/stub 
// pointers are set in the jumbo frame's packet array, 
// pointing within the data buffer
//----------------------------------------------------------
int
internal_compose_jumbo_frame(DAQFile *data, JumboFrame* frame,
			     int ID, int index)
//----------------------------------------------------------
{

#ifdef DEBUG
  printf("muedaq_build_jumbo_frame  : file=%s\n", data->filename);
  printf("muedaq_build_jumbo_frame  : frame=%llx\n", frame);
  printf("muedaq_build_jumbo_frame  : frame->buffer=%llx\n", frame->buffer);
#endif

  // reset 
  frame->buffer_ptr = &(frame->buffer[0]);
  frame->packet_ptr = &(frame->packet[0]);  
  
  //
  // fill the frame with data at the appropriate offset 
  // each time through the while loop is a new *LinkFrame* ...
  //
  int n_frame = 0, n_packet=0;
  LinkHeader lheader;
  while( !feof(data->fh) ) { 

    // read into a temporary link header
    fread(&lheader,1,8,data->fh); // 8x1B = 64b
    // translate into the jumbo header, overwriting it for this link frame
    frame->header.length = lheader.length;
    frame->header.code = lheader.code;    

    if( !feof(data->fh) && 
	(frame->header.code == HEADER_CODE_NORMAL ||
	 frame->header.code == HEADER_CODE_COMBINED ||
	 frame->header.code == HEADER_CODE_BUILT) ) { 

#ifdef DEBUG
      printf("muedaq_build_jumbo_frame : LinkFrame sequence=0x%llx\n", lheader.sequence);          
#endif
      
      // fill the payload buffer w/ link frame data 
      fread(frame->buffer_ptr,frame->header.length,8,data->fh);
      n_frame++;

      // drop in the pointers
      n_packet += assign_jumbo_frame_packet_ptrs( frame, ID, index );

      // check
      if( frame->packet_ptr - frame->packet != n_packet ) {
	printf("frame->packet_ptr  : 0x%llx\n", frame->packet_ptr );
	printf("frame->packet      : 0x%llx\n", frame->packet );
	printf("n_packet           : %d\n", n_packet );
	exit(1);
      }
      
      // update the buffer ptr
      (frame->buffer_ptr) += frame->header.length;            
      
#ifdef DEBUG
      printf("muedaq_build_jumbo_frame : %d packet pointers assigned\n", n_packet);          
#endif
      
    } // if valid frame
  } // while file 

#ifdef DEBUG
  printf("muedaq_build_jumbo_frame : read %d frames, assigned %d packet pointers\n", n_frame, n_packet);          
#endif

  // update frame's n_packet
  frame->n_packets = n_packet;
  if( !n_packet ) frame->packet_ptr = NULL;
  else frame->packet_ptr = frame->packet;
  return n_packet;
}

#endif 
