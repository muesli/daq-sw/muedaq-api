#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <getopt.h>

#include "muedaq_api.h"

enum { DUMP_METADATA, DUMP_HEADER, DUMP_PAYLOAD, DUMP_SUMMARY, FILTER} switches;
unsigned run_switches = 0;
char * filename;

void parse_args( int argc, char** argv ); // impl after main
void usage();
bool dump_metadata();
bool dump_header();
bool dump_payload();
bool dump_summary();
bool filter();
bool empty_packet();

//-----------------------------------------------------------------------
int
main(int argc, char** argv)
//-----------------------------------------------------------------------
{

  muedaq_print_version();
  
  // variables for decoding 
  // - metadata
  uint64_t m_run, m_seqerr;
  uint64_t m_start_seq, m_end_seq, m_tstamp;  
  uint64_t m_api_major, m_api_minor, m_api_patch;
  // - link frame header 
  unsigned f_code, f_len, f_run;
  uint64_t f_seq;
  // - packet header
  unsigned p_super, p_user, p_bxID, p_count;
  unsigned p_stat0, p_stat1, p_stat2, p_stat3;        
  // - stubs
  unsigned s_link, s_cic, s_cbc, s_fetype;
  unsigned s_bxoffset, s_bx, s_addr, s_z, s_bend;  


  parse_args(argc,argv);
  
  DAQFile data;
  muedaq_open_file(filename,&data);
#ifndef LEGACY_FORMAT
  muedaq_decode_metadata(&(data.metadata),
			 &m_run, &m_start_seq, &m_end_seq, &m_seqerr,
			 &m_api_major, &m_api_minor, &m_api_patch);  
#else
  muedaq_decode_metadata(&(data.metadata),
			 &m_run, &m_start_seq, &m_end_seq, &m_seqerr, &m_tstamp);  
#endif
  if( dump_metadata() ) { printf("\n"); muedaq_print_metadata(&(data.metadata));}

  
  unsigned fcnt=0,pcnt=0,scnt=0;
  unsigned long seq_prev;
  int bad_run_vs_meta=0, bad_seq_cnt=0;
  int ok_start_vs_meta=1, ok_end_vs_meta=1;

  LinkFrame frame;
  while ( muedaq_next_link_frame(&data,&frame) )
    {
      LinkHeader lheader = frame.header;
      muedaq_decode_link_header(&lheader,
				&f_code, &f_len, &f_run, &f_seq);
      if( dump_header() ) { 
	printf("\n\nlink header %7d\n", fcnt);
	printf("+++++++++++++++++++++++++++++++++\n");
	muedaq_print_link_header(&lheader);
      }

      //
      // optional link header checks 
      //
      // check run against metadata for every packet
      if( f_run != m_run ) { 
	printf( "ERROR :: run number does not match metadata : 0x%03llx (packet) vs 0x%03llx (metadata)\n", 
		f_run, m_run);
	bad_run_vs_meta++;
      }      
      // check for incrementing sequence
      if( fcnt == 0 ) {  // check pkt seq against metadata
	if( f_seq !=  m_start_seq ) {
	  printf( "ERROR :: bad initial sequence : 0x%09llx (packet) vs 0x%09llx (metadata)\n", 
		  f_seq, m_start_seq);
	  ok_start_vs_meta--; //
	}
      } else { // check against previous packets
	if( f_seq != seq_prev+1 ) { 
	  printf( "ERROR :: bad sequence : 0x%09llx vs expected 0x%09llx\n", 
		  f_seq, seq_prev+1);
	  bad_seq_cnt++;
	}
      }
      seq_prev = lheader.sequence;


      //
      // payload inspection
      //
      pcnt += frame.n_packets;

      for( int i=0; i<frame.n_packets; i++) {
	PayloadPacket* packet_ptr = frame.packet_ptr + i ;
	PayloadHeader* pheader_ptr = packet_ptr->header_ptr;

	// decode all header values ..
#ifndef LEGACY_FORMAT
	muedaq_decode_payload_header( pheader_ptr,
				      &p_super, &p_user, &p_bxID, &p_stat0, &p_stat1,
				      &p_stat2, &p_stat3, &p_count );
#else
	muedaq_decode_payload_header( pheader_ptr,
				      &p_super, &p_user, &p_stat0, &p_stat1,
				      &p_stat2, &p_stat3, &p_count );
#endif
	// do what you want with those values here ..
	
	// in this case, just dump direct from the header
	if( dump_payload() ) { 
	  printf("\n");
	  printf("packet header %3d  | ",i);
	  muedaq_print_payload_header(pheader_ptr);
	  printf("--------------------\n");
	}
	scnt += packet_ptr->n_stubs;
	for( int j=0; j<packet_ptr->n_stubs; j++) {
	  PayloadStub* pstub_ptr = packet_ptr->stub_ptr + j;
	  // decode everything from the stub
#ifndef LEGACY_FORMAT
	  muedaq_decode_payload_stub(pstub_ptr,
				     &s_link, &s_cic, &s_cbc, &s_fetype,
				     &s_bxoffset, &s_addr, &s_z, &s_bend);
#else
	  muedaq_decode_payload_stub(pstub_ptr,
				     &s_link, &s_cic, &s_cbc, 
				     &s_bx, &s_addr, &s_bend);
#endif 
	  // do what you want with those values here ..
	  
	  // in this case, just dump direct from the header 
	  if( dump_payload() ) { 
	    unsigned sraw = muedaq_payload_stub_to_raw(pstub_ptr);
	    if( sraw != 0 ) {
	      printf("\tstub %3d  (0x%08x) | ",j,sraw);      
	      muedaq_print_payload_stub(pstub_ptr);
	    }
	  }
	}
      }
      fcnt++;
    } // while   
  
  if( dump_summary() ) {
    printf("decode |  done\n");
    printf("       |  number of frames  : %d\n", fcnt);
    printf("       |  number of packets : %d\n", pcnt);
    printf("       |  number of stubs   : %d\n", scnt);    
    printf("       |  run number errors : %d\n", bad_run_vs_meta);
    printf("       |  sequence errors   : %d\n", bad_seq_cnt);
    printf("       |   metadata error flag : %d\n", data.metadata.seqerr);
    printf("       |   start sequence ok   : %d\n", ok_start_vs_meta);
    printf("       |   end sequence ok     : %d\n", ok_end_vs_meta);      
    printf("\n");
  }
}

//-----------------------------------------------------------------------
void
parse_args( int argc, char** argv )
//-----------------------------------------------------------------------
{

  if( argc<2 )  usage();
  
  int c;
  int digit_optind = 0;
  
  while (1) {

    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    static struct option long_options[] = {
					   {"metadata", no_argument, 0,  'm' },
					   {"header",   no_argument, 0,  'h' },
					   {"payload",  no_argument, 0,  'p' },
					   {"summary",  no_argument, 0,  's' },
					   {"filter",   no_argument, 0,  'f' },					   
					   {"help",     no_argument, 0,  '0' },					   
					   {0,          0,           0,  0 }
    };

    //printf("optind: %d\n", optind);
    
    c = getopt_long(argc, argv, "mhpsf",
		    long_options, &option_index);

    //have run out of args, exit the loop
    if (c == -1) { 
      if(run_switches == 0 ) {
	printf("decode |  full dump\n");
	run_switches |= (1<<DUMP_METADATA);
	run_switches |= (1<<DUMP_HEADER);
	run_switches |= (1<<DUMP_PAYLOAD);
	run_switches |= (1<<DUMP_SUMMARY);      	
      }
      break;
    }

    // have args, switch
    switch (c) {
    case 'm':
      printf("decode |  dumping metadata\n");
      run_switches |= 1<<DUMP_METADATA;
      run_switches |= 1<<DUMP_SUMMARY;      
      break;
    case 'h':
      printf("decode |  dumping header\n");
      run_switches |= 1<<DUMP_HEADER;
      run_switches |= 1<<DUMP_SUMMARY;            
      break;
    case 'p':
      printf("decode |  dumping payload\n");
      run_switches |= 1<<DUMP_PAYLOAD;
      run_switches |= 1<<DUMP_SUMMARY;            
      break;
    case 's':
      printf("decode |  dumping summary only\n");
      run_switches |= 1<<DUMP_SUMMARY;            
      break;
    case 'f':
      printf("decode |  filtering\n");
      run_switches |= 1<<FILTER;            
      break;            
    case '0':
      usage();
      break;
    case '?':
      break;
      
    default:
      printf("?? getopt returned character code 0%o ??\n", c);
    }
  }

  // non opt args fall through 
  if (optind < argc) {
    //printf("optind: %d, argc: %d\n", optind, argc);
    if( optind <= argc-2 ) {
      printf("error: decode accepts only one file argument\n");
      usage();
      exit(EXIT_FAILURE);
    }
    /*
    printf("non-option ARGV-elements: ");
    while (optind < argc)
      printf("%s ", argv[optind++]);
    printf("\n");
    */
    filename = argv[optind];
    printf( "decode |  data file = %s\n", filename);
  }

  //exit(EXIT_SUCCESS);
}


//-----------------------------------------------------------------------
void
usage()
//-----------------------------------------------------------------------
{
  printf("\n-- decode help -- \n\n");
  printf("decode [OPTIONS] FILE\n");
  printf("\n");
  printf("DESCRIPTION:\n");
  printf("By default, the metadata and headers are decoded, and the payload packets are dumped.  Consistency checks \n");
  printf("are performed on the packet sequence and run numbers.  Printouts of the three section can be separately \n");
  printf("enables with the options below.\n");
  printf("\n");
  printf("OPTIONS:\n");
  printf("--metadata (-m) : print file metadata\n");
  printf("--headers  (-h) : print packet headers\n");
  printf("--payload  (-p) : print payload data\n");
  printf("--summary  (-s) : print file summary only\n");
  printf("--filter   (-f) : filter empty packets\n");  
  printf("--help          : this message\n");        
  exit(0);
}


//-----------------------------------------------------------------------
bool
dump_metadata() 
//-----------------------------------------------------------------------
{
  return run_switches & (1<<DUMP_METADATA);
}

//-----------------------------------------------------------------------
bool
dump_header() 
//-----------------------------------------------------------------------
{
  return run_switches & (1<<DUMP_HEADER);
}

//-----------------------------------------------------------------------
bool
dump_payload() 
//-----------------------------------------------------------------------
{
  return run_switches & (1<<DUMP_PAYLOAD);
}

//-----------------------------------------------------------------------
bool
dump_summary() 
//-----------------------------------------------------------------------
{
  return run_switches & (1<<DUMP_SUMMARY);
}

//-----------------------------------------------------------------------
bool
filter() 
//-----------------------------------------------------------------------
{
  return run_switches & (1<<FILTER);
}


//-----------------------------------------------------------------------
bool
empty_packet(uint64_t* ptr, uint64_t len) 
//-----------------------------------------------------------------------
{
  bool nonempty = false, tmp;
  for( int i=0; i<len; i++ ) {
    switch (i%4)
      {
      case 1:
	// not necessarily zero for and empty packet ...
	//nonempty |= !(ptr[i] == 0);
	break;
      case 2:
	// not necessarily zero for and empty packet ...
	// nonempty |= !(ptr[i] == 0);
	break;
      case 3:
	nonempty |= !(ptr[i] == PACKET_DELIMITER);
	break;
      default :
	break;
      }
  }

  return !nonempty;
}
