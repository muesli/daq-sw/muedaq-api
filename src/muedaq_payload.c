#include <stdio.h>
#include "muedaq_payload.h"


//
// encode/decode/dump format 
//

// format independent

//----------------------------------------------------------
uint64_t* 
muedaq_payload_header_to_raw( PayloadHeader * hdrptr )
//----------------------------------------------------------
{
  return (uint64_t*)hdrptr;
}

//----------------------------------------------------------
PayloadHeader * 
muedaq_raw_to_payload_header( uint64_t* ptr )
//----------------------------------------------------------
{
  return (PayloadHeader*)ptr;
}

//----------------------------------------------------------
unsigned 
muedaq_payload_stub_to_raw( PayloadStub * stubptr)
//----------------------------------------------------------
{
  return *((unsigned*)stubptr);
}

//----------------------------------------------------------
PayloadStub * 
muedaq_raw_to_payload_stub( unsigned * ptr)
//----------------------------------------------------------
{
  return (PayloadStub*)ptr;
}


// -- v1 format
#ifndef LEGACY_FORMAT


//----------------------------------------------------------
void
muedaq_decode_payload_header( PayloadHeader * hdrptr,
			      unsigned * bx_super_ID,
			      unsigned * user_data,
			      unsigned * bx_ID,			      
			      unsigned * module_0_status,
			      unsigned * module_1_status,
			      unsigned * module_2_status,
			      unsigned * module_3_status,
			      unsigned * stub_count    )
//----------------------------------------------------------
{
  *bx_super_ID       = muedaq_payload_header_get_superID(hdrptr);
  *user_data         = muedaq_payload_header_get_userdata(hdrptr);
  *bx_ID             = muedaq_payload_header_get_bxID(hdrptr);  
  *module_0_status   = muedaq_payload_header_get_status(hdrptr,0);
  *module_1_status   = muedaq_payload_header_get_status(hdrptr,1);
  *module_2_status   = muedaq_payload_header_get_status(hdrptr,2);
  *module_3_status   = muedaq_payload_header_get_status(hdrptr,3);
  *stub_count        = muedaq_payload_header_get_stubcount(hdrptr);
  return;
}


//----------------------------------------------------------
void
muedaq_encode_payload_header( PayloadHeader * hdrptr,
			      unsigned  bx_super_ID,
			      unsigned  user_data,
			      unsigned  bx_ID,			      
			      unsigned  module_0_status,
			      unsigned  module_1_status,
			      unsigned  module_2_status,
			      unsigned  module_3_status,
			      unsigned  stub_count    )
//----------------------------------------------------------
{
  hdrptr->bx_super_ID     = bx_super_ID;       
  hdrptr->user_data       = user_data;         
  hdrptr->bx_ID           = bx_ID;
  
  hdrptr->module_0_status = module_0_status;   
  hdrptr->module_1_status = module_1_status;   
  hdrptr->module_2_status = module_2_status;   
  hdrptr->module_3_status = module_3_status;   

  hdrptr->stub_count      = stub_count;
  return;
}


//----------------------------------------------------------
void
muedaq_print_payload_header( PayloadHeader * hdrptr )
//----------------------------------------------------------
{
  unsigned super, user, bx, stat0, stat1, stat2, stat3, nstub;
  muedaq_decode_payload_header(hdrptr,
			       &super, &user, &bx,
			       &stat0, &stat1, &stat2, &stat3,
			       &nstub);
  printf( "super: 0x%08x\t"  ,super);
  printf( "usr: 0x%08x\t"    ,user);
  printf( "bxID: 0x%01x\t"   ,bx);  
  printf( "stat0: 0x%05x\t"  ,stat0);
  printf( "stat1: 0x%05x\t"  ,stat1);
  printf( "stat2: 0x%05x\t"  ,stat2);
  printf( "stat3: 0x%05x\t"  ,stat3);
  printf( "nstub: 0x%02x\t"  ,nstub);
  printf("\n");
  return;
}


//----------------------------------------------------------
void
muedaq_decode_payload_stub( PayloadStub * stubptr,
			    unsigned * link,
			    unsigned * cic,
			    unsigned * cbc,
			    unsigned * fe_type,			    
			    unsigned * bx_offset,
			    unsigned * addr,
			    unsigned * z,
			    unsigned * bend )
//----------------------------------------------------------
{
  *link        = stubptr->link_ID;
  *cic         = stubptr->cic_ID;
  *cbc         = stubptr->cbc_ID;
  *fe_type     = stubptr->FE_type;  
  *bx_offset   = stubptr->bx_offset;  
  *addr        = stubptr->addr;
  *z           = stubptr->z;
  *bend        = stubptr->bend;      
  return;
}


//----------------------------------------------------------
void
muedaq_encode_payload_stub( PayloadStub * stubptr,
			    unsigned  link,
			    unsigned  cic,
			    unsigned  cbc,
			    unsigned  fe_type,			    
			    unsigned  bx_offset,
			    unsigned  addr,
			    unsigned  z,			    
			    unsigned  bend )
//----------------------------------------------------------
{
  stubptr->link_ID   = link;
  stubptr->cic_ID    = cic;  
  stubptr->cbc_ID    = cbc;
  stubptr->FE_type   = fe_type;    
  stubptr->bx_offset = bx_offset;   
  stubptr->addr      = addr; 
  stubptr->z         = z;
  stubptr->bend      = bend;         
  return;
}


//----------------------------------------------------------
void
muedaq_print_payload_stub( PayloadStub * stubptr )
//----------------------------------------------------------
{
  unsigned z, bend, addr, bx_offset, fe_type, cbc, cic, link;
  muedaq_decode_payload_stub(stubptr,&link,&cic,&cbc,&fe_type,&bx_offset,&addr,&z,&bend);
  printf("link: 0x%1x\t"       ,link);                                          
  printf("cic: 0x%1x\t"        ,cic);
  printf("cbc: 0x%1x\t"        ,cbc);
  printf("fe_type: 0x%1x\t"    ,fe_type);  
  printf("bx_offset: 0x%01x\t" ,bx_offset);
  printf("addr: 0x%02x\t"      ,addr);
  printf("z: 0x%1x\t"          ,z);
  printf("bend: 0x%1x\t"       ,bend);
  printf("\n");
  return;
}





// -- v0 format 
#else




//----------------------------------------------------------
void
muedaq_decode_payload_header( PayloadHeader * hdrptr,
			      unsigned * bx_super_ID,
			      unsigned * user_data,
			      unsigned * module_0_status,
			      unsigned * module_1_status,
			      unsigned * module_2_status,
			      unsigned * module_3_status,
			      unsigned * stub_count    )
//----------------------------------------------------------
{
  *bx_super_ID       = muedaq_payload_header_get_superID(hdrptr);
  *user_data         = muedaq_payload_header_get_userdata(hdrptr);
  *module_0_status   = muedaq_payload_header_get_status(hdrptr,0);
  *module_1_status   = muedaq_payload_header_get_status(hdrptr,1);
  *module_2_status   = muedaq_payload_header_get_status(hdrptr,2);
  *module_3_status   = muedaq_payload_header_get_status(hdrptr,3);
  *stub_count        = muedaq_payload_header_get_stubcount(hdrptr);
  return;
}


//----------------------------------------------------------
void
muedaq_encode_payload_header( PayloadHeader * hdrptr,
			      unsigned  bx_super_ID,
			      unsigned  user_data,
			      unsigned  module_0_status,
			      unsigned  module_1_status,
			      unsigned  module_2_status,
			      unsigned  module_3_status,
			      unsigned  stub_count    )
//----------------------------------------------------------
{
  hdrptr->bx_super_ID     = bx_super_ID;       
  hdrptr->user_data       = user_data;         
  /*
  hdrptr->module_0_status = module_0_status;   
  hdrptr->module_1_status = module_1_status;   
  hdrptr->module_2_status = module_2_status;   
  hdrptr->module_3_status = module_3_status;   
  */
  // update for correct status ordering -- Jun'22
  hdrptr->module_0_cic_0_status = module_0_status&0x1FF;
  hdrptr->module_0_cic_1_status = (module_0_status>>9)&0x1FF;
  hdrptr->module_1_cic_0_status = module_1_status&0x1FF;
  hdrptr->module_1_cic_1_status = (module_1_status>>9)&0x1FF;
  hdrptr->module_2_cic_0_status = module_2_status&0x1FF;
  hdrptr->module_2_cic_1_status = (module_2_status>>9)&0x1FF;
  hdrptr->module_3_cic_0_status = module_3_status&0x1FF;
  hdrptr->module_3_cic_1_status = (module_3_status>>9)&0x1FF;           
  //
  hdrptr->stub_count      = stub_count;
  return;
}


//----------------------------------------------------------
void
muedaq_print_payload_header( PayloadHeader * hdrptr )
//----------------------------------------------------------
{
  unsigned super, user, stat0=0, stat1=0, stat2=0, stat3=0, nstub;
  muedaq_decode_payload_header(hdrptr,
			       &super, &user,
			       &stat0, &stat1, &stat2, &stat3,
			       &nstub);
  printf( "super: 0x%08x\t" ,super);
  printf( "usr: 0x%08x\t"   ,user);
  printf( "stat0: 0x%05x\t" ,stat0);
  printf( "stat1: 0x%05x\t" ,stat1);
  printf( "stat2: 0x%05x\t" ,stat2);
  printf( "stat3: 0x%05x\t" ,stat3);
  printf( "nstub: 0x%02x\t" ,nstub);
  printf("\n");
  return;
}

//----------------------------------------------------------
void
muedaq_decode_payload_stub( PayloadStub * stubptr,
			    unsigned * link,
			    unsigned * cic,
			    unsigned * cbc,
			    unsigned * bx,
			    unsigned * addr,
			    unsigned * bend )			    			    			    
//----------------------------------------------------------
{
  *link = stubptr->link_ID;
  *cic  = stubptr->cic_ID;
  *cbc  = stubptr->cbc_ID;
  *bx   = stubptr->bx;
  *addr = stubptr->addr;
  *bend = stubptr->bend;      
  return;
}


//----------------------------------------------------------
void
muedaq_encode_payload_stub( PayloadStub * stubptr,
			    unsigned  link,
			    unsigned  cic,
			    unsigned  cbc,
			    unsigned  bx,
			    unsigned  addr,
			    unsigned  bend )
//----------------------------------------------------------
{
  stubptr->link_ID = link;
  stubptr->cic_ID  = cic;  
  stubptr->cbc_ID  = cbc;  
  stubptr->bx	   = bx;   
  stubptr->addr    = addr; 
  stubptr->bend    = bend;       
  return;
}


//----------------------------------------------------------
void
muedaq_print_payload_stub( PayloadStub * stubptr )
//----------------------------------------------------------
{
  unsigned bend, addr, cbc, bx, cic, link;
  muedaq_decode_payload_stub(stubptr,&link,&cic,&cbc,&bx,&addr,&bend);
  printf("link: 0x%1x\t"  ,link);                                          
  printf("cic: 0x%1x\t"   ,cic);
  printf("cbc: 0x%1x\t"   ,cbc);
  printf("bx: 0x%03x\t"   ,bx);
  printf("addr: 0x%02x\t" ,addr);
  printf("bend: 0x%1x\t"  ,bend);
  printf("\n");
  return;
}

#endif





//
// accessors
//


// -- format independent 

uint32_t muedaq_payload_header_get_superID(PayloadHeader * hptr)
{
  return hptr->bx_super_ID;
}
uint32_t muedaq_payload_header_get_userdata(PayloadHeader * hptr)
{
  return hptr->user_data;
}
uint32_t muedaq_payload_header_get_stubcount(PayloadHeader * hptr)
{
  return hptr->stub_count; 
}
uint32_t muedaq_payload_stub_get_bend(PayloadStub * sptr)
{
  return sptr->bend;      
}
uint32_t muedaq_payload_stub_get_addr(PayloadStub * sptr)
{
  return sptr->addr;
}
uint32_t muedaq_payload_stub_get_cbcID(PayloadStub * sptr)
{
  return sptr->cbc_ID;
}
uint32_t muedaq_payload_stub_get_linkID(PayloadStub * sptr)
{
  return sptr->link_ID;
}
uint32_t muedaq_payload_stub_get_cicID(PayloadStub * sptr)
{
  return sptr->cic_ID;
}

// --format dependent : v1
#ifndef LEGACY_FORMAT

uint32_t muedaq_payload_header_get_status(PayloadHeader * hptr, int module)
{
  uint32_t status;
  uint64_t cic;

  switch(module) { 
  case 0:
    cic = hptr->module_0_status;
    break;
  case 1:
    cic = hptr->module_1_status;
    break;      
  case 2:
    cic = hptr->module_2_status;
    break;
  case 3:
    cic = hptr->module_3_status;
    break;      
  }
  return cic;
}


uint32_t muedaq_payload_header_get_bxID(PayloadHeader * hptr)
{
  return hptr->bx_ID;
}
uint32_t muedaq_payload_stub_get_z(PayloadStub * sptr)
{
  return sptr->z;      
}

uint32_t muedaq_payload_stub_get_bxOffset(PayloadStub * sptr)
{
  return sptr->bx_offset;
}

uint32_t muedaq_payload_stub_get_FEtype(PayloadStub * sptr)
{
  return sptr->FE_type;
}


// --format dependent : v0
#else


uint32_t muedaq_payload_header_get_status(PayloadHeader * hptr, int module)
{
  uint32_t status=0;
  uint64_t cic1=0, cic0=0;

  switch(module) { 
  case 0:
    cic1 = hptr->module_0_cic_1_status;
    cic0 = hptr->module_0_cic_0_status;
    break;
  case 1:
    cic1 = hptr->module_1_cic_1_status;
    cic0 = hptr->module_1_cic_0_status;
    break;      
  case 2:
    cic1 = hptr->module_2_cic_1_status;
    cic0 = hptr->module_2_cic_0_status;
    break;
  case 3:
    cic1 = hptr->module_3_cic_1_status;
    cic0 = hptr->module_3_cic_0_status;
    break;      
  }
  status = (cic1<<9UL)|(cic0);

  return status;
}

uint32_t muedaq_payload_stub_get_bx(PayloadStub * sptr)
{
  return sptr->bx;
}

#endif




