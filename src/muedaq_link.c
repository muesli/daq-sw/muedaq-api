#include <stdio.h>
#include "muedaq_link.h"


//
// internal routines 
//


//----------------------------------------------------------
// this drops in packet/stub pointers to a loaded link frmae
// buffer into the packet array
//----------------------------------------------------------
int
assign_link_frame_packet_ptrs( LinkFrame* frame )
//----------------------------------------------------------
{
  const int kPayloadHeaderSize_w64 = sizeof(PayloadHeader)/sizeof(uint64_t);

  // start at the beginning
  uint64_t* buffer     = frame->buffer;
  uint64_t* buffer_ptr = frame->buffer_ptr;  

#ifdef DEBUG
  printf("assign_link_frame_packet_ptrs : loading %d uint64, frame=0x%llx, packet=0x%llx\n",
  	 frame->header.length,frame,&(frame->packet));
  #endif

  // loop over all of the packet data for this link frame 
  int wcnt=0, pcnt=0;
  while( wcnt < frame->header.length-1  )
    {
      // point to the packet header
      frame->packet_ptr->header_ptr = (PayloadHeader*)(buffer_ptr);
      buffer_ptr += kPayloadHeaderSize_w64;    
      wcnt += kPayloadHeaderSize_w64;                
      
#ifdef DEBUG
      printf("assign_link_frame_packet_ptrs : packet header superID=0x%llx\n",
	     frame->packet_ptr->header_ptr->bx_super_ID );
#endif
  
      // point to the stubs
      int scnt=0,wcnt32=0;
      uint32_t* u32p = (uint32_t*)(buffer_ptr);
      frame->packet_ptr->stub_ptr = (PayloadStub*)(u32p);
      // intermediate++, see below
      uint32_t *u32p_first_nonzero, *u32p_last_nonzero;
  
      // stub data loop, no -1 so as to reach the delimiter
      while( *u32p != (PACKET_DELIMITER&0xFFFFFFFFU) && wcnt < frame->header.length  )
	{ 
	  if( *u32p != 0 ) { 
	    
	    // old case : add stub by copying
	    //pktarr_ptr->stub_ptr[scnt] = (PayloadStub*)(u32p);
	    
	    // intermediate case : just count non-zero stubs, copy can miss stubs
	    // if zero-stubs are in between
	    if( scnt == 0 )  u32p_first_nonzero = u32p;
	    scnt++;
	    
	    // intermediate++ : go until the last non-zero stub.  
	    // everything in between will be copied see below ...
	    u32p_last_nonzero = u32p;
	  } 
	  
	  wcnt += wcnt32%2; // advance in buffer
	  (buffer_ptr) += wcnt32%2; // advance in buffer
	  wcnt32++;
	  u32p++;
	}
      
      // intermediate++ : include zero-stubs
      if( scnt > 0 ) { 
	scnt = u32p_last_nonzero - u32p_first_nonzero +1 ; //override scnt
	frame->packet_ptr->stub_ptr =  (PayloadStub*)u32p_first_nonzero;
#ifdef DEBUG
	printf("assign_packet_ptrs : packet %d n_stubs set to %d\n",
	       pcnt, scnt);
#endif
      } else {
	frame->packet_ptr->stub_ptr = NULL;
      }

      frame->packet_ptr->n_stubs = scnt;  // note stub couunt
      
      pcnt++;                   // we found a packet
      (frame->packet_ptr) += 1; // move to the next packet pointer
      wcnt++;                   // advance in buffer, past the delimiter
      (buffer_ptr) += 1 ;
    } // loop over buffer data

  frame->n_packets += pcnt;
  return pcnt;
}


//----------------------------------------------------------
// This is called by muedaq_next_link_frame, which has 
// already read in the frame header and copies the payload 
// data to the internal buffer
//----------------------------------------------------------
int
internal_load_link_frame(DAQFile *data, LinkFrame* frame)
//----------------------------------------------------------
{

  // reset 
  frame->buffer_ptr = &(frame->buffer[0]);
  frame->packet_ptr = &(frame->packet[0]);  
  
  //
  // fill the frame with data at the appropriate offset 
  // while loop is case we need to read ahead ...
  //
  int n_frame = 0, n_packet=0;
  while( !feof(data->fh) ) { 
    
    // read in the link header
    fread(&frame->header,1,8,data->fh); // 8x1B = 64b
    
    if( !feof(data->fh) && 
	(frame->header.code == HEADER_CODE_NORMAL ||
	 frame->header.code == HEADER_CODE_COMBINED ) ) {
      
#ifdef DEBUG
      printf("internal_load_link_frame : sequence=0x%llx\n", frame->header.sequence);          
#endif
      
      // fill the payload buffer w/ 64b link frame data 
      fread(frame->buffer_ptr,frame->header.length,8,data->fh);
      n_frame++;
      
      // drop in the pointers
      n_packet = assign_link_frame_packet_ptrs( frame );
      
      // check
      if( frame->packet_ptr - frame->packet != n_packet ) {
	printf("frame->packet_ptr  : 0x%llx\n", frame->packet_ptr );
	printf("frame->packet      : 0x%llx\n", frame->packet );
	printf("n_packet           : %d\n", n_packet );
	exit(1);
      }
      
      
#ifdef DEBUG
      printf("internal_load_link_frame : %d packet pointers assigned\n", n_packet);          
#endif
      
    } // if valid frame
    
    if( n_frame ) break; // just want one frame
  }

  // update frame's n_packet
  frame->n_packets = n_packet;
  if( !n_packet ) frame->packet_ptr = NULL;
  else frame->packet_ptr = frame->packet;
  return n_packet;
}





//
// exposed by the API
//




//----------------------------------------------------------
bool
muedaq_next_link_frame(DAQFile * data, LinkFrame * frame)
//----------------------------------------------------------
{
  int ret = internal_load_link_frame(data,frame) ;
  if ( 0 < ret ) {
    frame->buffer_ptr = frame->buffer;
    frame->packet_ptr = frame->packet;
    return true;
  }
  frame->buffer_ptr = frame->buffer;
  frame->packet_ptr = NULL;
  return false;
}





//----------------------------------------------------------
void
muedaq_decode_link_header(LinkHeader * lhdr,
			  unsigned * pkt_code,
			  unsigned * pkt_len,
			  unsigned * pkt_run,
			  uint64_t * pkt_seq)
//----------------------------------------------------------
{
  /*
  *pkt_code = lhdr->code;
  *pkt_len  = lhdr->length;
  *pkt_run  = lhdr->run;
  *pkt_seq  = lhdr->sequence;
  */
  // with acessors
  *pkt_code = muedaq_link_get_code(lhdr);
  *pkt_len  = muedaq_link_get_length(lhdr);
  *pkt_run  = lhdr->run;
  *pkt_seq  = lhdr->sequence;  
  return;
}


//----------------------------------------------------------
void
muedaq_encode_link_header(LinkHeader * lhdr,
			  unsigned  pkt_code,
			  unsigned  pkt_len,
			  unsigned  pkt_run,
			  uint64_t  pkt_seq)
//----------------------------------------------------------
{
  lhdr->code     = pkt_code;
  lhdr->length   = pkt_len;  
  lhdr->run      = pkt_run;  
  lhdr->sequence = pkt_seq; 
  return;
}


//----------------------------------------------------------
uint64_t
muedaq_link_header_to_raw(LinkHeader* lhdr)
//----------------------------------------------------------
{
  return *((uint64_t*)lhdr);
}


//----------------------------------------------------------
LinkHeader *
muedaq_raw_to_link_header(uint64_t* raw)
//----------------------------------------------------------
{
  return (LinkHeader*)raw;
}


//----------------------------------------------------------
void
muedaq_print_link_header(LinkHeader* lhdr)
//----------------------------------------------------------
{
  uint64_t sequence;
  unsigned code,length,run;
  muedaq_decode_link_header(lhdr,&code,&length,&run,&sequence);

  char * code_str;
  if( code == HEADER_CODE_NORMAL )         code_str="normal";
  else if( code == HEADER_CODE_COMBINED )  code_str="aggregate";
  else if( code == HEADER_CODE_HEARTBEAT ) code_str="heartbeat";  
  else code_str="invalid";
  
  printf( "header code    : 0x%2x (%s)\n", code,code_str);
  printf( "payload length : 0x%2x\n",      length);      
  printf( "run#           : 0x%3x\n",      run);      
  printf( "sequence#      : 0x%09llx\n",   sequence);
  return; 
}

//
// accessors
//
uint64_t muedaq_link_get_run(LinkHeader * lptr)
{
  return lptr->run;
}
uint64_t muedaq_link_get_sequence(LinkHeader * lptr)
{
  return lptr->sequence;
}
uint64_t muedaq_link_get_length(LinkHeader * lptr)
{
  return lptr->length;
}
uint64_t muedaq_link_get_code(LinkHeader * lptr)
{
  return lptr->code;
}
