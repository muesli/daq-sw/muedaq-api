#ifndef MUEDAQ_PAYLOAD
#define MUEDAQ_PAYLOAD

#include <ctype.h>
#include <arpa/inet.h>

#define MUEDAQ_MAX_PAYLOAD_STUBS   128

#ifdef __cplusplus
extern "C" {
#endif
  
#ifndef LEGACY_FORMAT

  //
  // definitions : v1 format
  //
  
  typedef struct {
    uint32_t bx_super_ID; // 31-0
    uint32_t user_data;   // 63-32
    //  
    uint64_t module_0_status : 18; // 17-0
    uint64_t module_1_status : 18; // 35-18
    uint64_t bx_ID           : 12; // 47-36
    uint64_t rsvd1           : 8;  // 55-48       
    uint64_t stub_count      : 8;  // 63-56        
    //
    uint64_t module_2_status : 18; // 17-0
    uint64_t module_3_status : 18; // 35-18
    uint64_t rsvd2           : 28; // 63-36        
  } PayloadHeader;
  
  typedef struct {
    uint32_t z         : 4;  //  3-0
    uint32_t bend      : 4;  //  7-4
    uint32_t addr      : 8;  // 15-8
    uint32_t cbc_ID    : 3;  // 18-16
    uint32_t bx_offset : 3;  // 21-19
    uint32_t cic_ID    : 1;  // 22
    uint32_t FE_type   : 1;  // 23  
    uint32_t link_ID   : 4;  // 27-24
    uint32_t rcvd      : 4;  // 31-28
  } PayloadStub;
  
  
  
#else


  //
  // definitions : v0 format
  //
  
  typedef struct {
    uint32_t bx_super_ID; // 31-0
    uint32_t user_data;   // 63-32
    //  
    uint64_t module_0_cic_1_status : 9; // 8-0
    uint64_t module_1_cic_1_status : 9; // 17-9
    uint64_t module_2_cic_1_status : 9; // 26-18
    uint64_t module_3_cic_1_status : 9; // 35-27  
    uint64_t module_0_cic_0_status : 9; // 44-36
    uint64_t module_1_cic_0_status : 9; // 53-45   
    uint64_t rsvd1                 : 2;  // 55-54        
    uint64_t stub_count            : 8;  // 63-56        
    //
    uint64_t module_2_cic_0_status : 9; // 8-0
    uint64_t module_3_cic_0_status : 9; // 17-9
    uint64_t rsvd2                 : 46; // 63-18        
  } PayloadHeader;
  
  
  typedef struct {
    uint32_t bend     : 4;  //  3-0
    uint32_t addr     : 8;  // 11-4
    uint32_t cbc_ID   : 3;  // 14-12
    uint32_t bx       : 12; // 26-15
    uint32_t cic_ID   : 1;  // 27
    uint32_t link_ID  : 4;  // 31-28  
  } PayloadStub;
  
#endif 
  
  
  //  
  // definitions : independent of format 
  //

  typedef struct {
    PayloadHeader  *header_ptr;
    PayloadStub    *stub_ptr; // ptr to the first stub                          
    int            n_stubs;
  } PayloadPacket;
  
  




  
  //
  // prototypes : encode/decode/dump
  //



  // format independent
  uint64_t*        muedaq_payload_header_to_raw ( PayloadHeader * hdrptr );
  PayloadHeader*   muedaq_raw_to_payload_header ( uint64_t* ptr );
  void             muedaq_print_payload_header  ( PayloadHeader * hdrptr );
  unsigned         muedaq_payload_stub_to_raw   ( PayloadStub * stubptr);
  PayloadStub *    muedaq_raw_to_payload_stub   ( unsigned * ptr);
  void             muedaq_print_payload_stub    ( PayloadStub * stubptr );
  
  // - v1 format
#ifndef LEGACY_FORMAT

  void muedaq_decode_payload_header             ( PayloadHeader * hdrptr,
						  unsigned * bx_super_ID,
						  unsigned * user_data,
						  unsigned * bx_ID,			      
						  unsigned * module_0_status,
						  unsigned * module_1_status,
						  unsigned * module_2_status,
						  unsigned * module_3_status,
						  unsigned * stub_count    );
  void muedaq_encode_payload_header             ( PayloadHeader * hdrptr,
						  unsigned  bx_super_ID,
						  unsigned  user_data,
						  unsigned  bx_ID,			      
						  unsigned  module_0_status,
						  unsigned  module_1_status,
						  unsigned  module_2_status,
						  unsigned  module_3_status,
						  unsigned  stub_count    );
  void muedaq_decode_payload_stub               ( PayloadStub * stubptr,
						  unsigned * link,
						  unsigned * cic,
						  unsigned * cbc,
						  unsigned * fe_type,			    
						  unsigned * bx_offset,
						  unsigned * addr,
						  unsigned * z,
						  unsigned * bend );
  void muedaq_encode_payload_stub               ( PayloadStub * stubptr,
						  unsigned  link,
						  unsigned  cic,
						  unsigned  cbc,
						  unsigned  fe_type,			    
						  unsigned  bx_offset,
						  unsigned  addr,
						  unsigned  z,			    
						  unsigned  bend );  
  // - v0 format  
  #else

  void muedaq_decode_payload_header             ( PayloadHeader * hdrptr,
						  unsigned * bx_super_ID,
						  unsigned * user_data,
						  unsigned * module_0_status,
						  unsigned * module_1_status,
						  unsigned * module_2_status,
						  unsigned * module_3_status,
						  unsigned * stub_count    );
  void muedaq_encode_payload_header             ( PayloadHeader * hdrptr,
						  unsigned  bx_super_ID,
						  unsigned  user_data,
						  unsigned  module_0_status,
						  unsigned  module_1_status,
						  unsigned  module_2_status,
						  unsigned  module_3_status,
						  unsigned  stub_count    );
  void muedaq_decode_payload_stub               ( PayloadStub * stubptr,
						  unsigned * link,
						  unsigned * cic,
						  unsigned * cbc,
						  unsigned * bx,
						  unsigned * addr,
						  unsigned * bend );
  void muedaq_encode_payload_stub               ( PayloadStub * stubptr,
						  unsigned  link,
						  unsigned  cic,
						  unsigned  cbc,
						  unsigned  bx,
						  unsigned  addr,
						  unsigned  bend );

#endif




  
  //
  // accessors
  //
  
  
  // independent of format 
  
  uint32_t muedaq_payload_header_get_superID   (PayloadHeader *);
  uint32_t muedaq_payload_header_get_userdata  (PayloadHeader *);
  uint32_t muedaq_payload_header_get_status    (PayloadHeader *, int);
  uint32_t muedaq_payload_header_get_stubcount (PayloadHeader *);
  
  uint32_t muedaq_payload_stub_get_bend        (PayloadStub *);
  uint32_t muedaq_payload_stub_get_addr        (PayloadStub *);
  uint32_t muedaq_payload_stub_get_cbcID       (PayloadStub *);
  uint32_t muedaq_payload_stub_get_cicID       (PayloadStub *);
  uint32_t muedaq_payload_stub_get_linkID      (PayloadStub *);
  
#ifndef LEGACY_FORMAT
  // format dependent, v1
  uint32_t muedaq_payload_stub_get_z           (PayloadStub *);
  uint32_t muedaq_payload_header_get_bxID      (PayloadHeader *);
  uint32_t muedaq_payload_stub_get_bxOffset    (PayloadStub *);
  uint32_t muedaq_payload_stub_get_FEtype      (PayloadStub *);
  // format dependent, v0
#else
  uint32_t muedaq_payload_stub_get_bx          (PayloadStub *);
#endif 
  
  
#ifdef __cplusplus
}
#endif

#endif

