#ifndef MUEDAQ_LINK
#define MUEDAQ_LINK

#include <ctype.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdbool.h>

#include "muedaq_file.h"
#include "muedaq_payload.h"

#define HEADER_CODE_POS       7
#define HEADER_CODE_NORMAL    0xAA
#define HEADER_CODE_COMBINED  0xAC
#define HEADER_CODE_HEARTBEAT 0xDD
#define HEADER_LENGTH_POS     6

#define MUEDAQ_MAX_PAYLOAD_WORDS   255
#define MUEDAQ_MAX_PAYLOAD_PACKETS 255

#ifdef __cplusplus
extern "C" {
#endif


  //
  // definitions
  //
  

  typedef struct {
    uint64_t sequence   : 36; // 35-0
    uint64_t run        : 12; // 47-36
    uint64_t length     : 8;  // 55-48
    uint64_t code       : 8;  // 63-56          
  } LinkHeader;
  
  typedef struct {
    
    LinkHeader      header;  
    
    PayloadPacket*  packet_ptr;
    PayloadPacket   packet[MUEDAQ_MAX_PAYLOAD_PACKETS];
    
    int             n_packets; 
    
    uint64_t*       buffer_ptr;  
    uint64_t        buffer[MUEDAQ_MAX_PAYLOAD_WORDS];
    
  } LinkFrame;
  

  
  //
  // prototpyes : encode/decode/dump
  //

  bool           muedaq_next_link_frame    ( DAQFile * data, LinkFrame * frame);
  void           muedaq_encode_link_header ( LinkHeader* lhdr, unsigned pkt_code,
					     unsigned pkt_len, unsigned pkt_run,
					     uint64_t pkt_seq );
  void           muedaq_decode_link_header ( LinkHeader* lhdr, unsigned* pkt_code,
					     unsigned* pkt_len, unsigned* pkt_run,
					     uint64_t* pkt_seq );
  uint64_t       muedaq_link_header_to_raw ( LinkHeader * lhdr );
  LinkHeader*    muedaq_raw_to_link_header ( uint64_t* raw );
  void           muedaq_print_link_header  ( LinkHeader* lhdr );

  
  //
  // prototype : header accessors
  //

  uint64_t       muedaq_link_get_run       (LinkHeader *);
  uint64_t       muedaq_link_get_sequence  (LinkHeader *);
  uint64_t       muedaq_link_get_length    (LinkHeader *);
  uint64_t       muedaq_link_get_code      (LinkHeader *);
  
#ifdef __cplusplus
}
#endif

#endif

