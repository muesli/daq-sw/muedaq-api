#ifndef MUEDAQ_API
#define MUEDAQ_API

#include "muedaq_payload.h"
#include "muedaq_link.h"
#include "muedaq_evtbuilder.h"
#include "muedaq_metadata.h"
#include "muedaq_file.h"
//#include "muedaq_version.h"


#ifdef __cplusplus
extern "C" {
#endif

  void           muedaq_print_version         ();
  const char *   muedaq_api_get_tag           ();
  void           muedaq_get_api_version(const char *,
					uint64_t*,
					uint64_t*,
					uint64_t*);        
#ifdef __cplusplus
}
#endif
  
#endif

