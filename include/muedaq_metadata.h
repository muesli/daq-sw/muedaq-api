#ifndef MUEDAQ_METADATA
#define MUEDAQ_METADATA

#include <ctype.h>
#include <arpa/inet.h>

#define METADATA_WORDS 2

#ifdef __cplusplus
extern "C" {
#endif


  //
  // defintions
  //

#ifndef LEGACY_FORMAT  
  
  typedef struct {
    uint64_t start_seq  : 36; // 35-0
    uint64_t run        : 12; // 49-36      
    uint64_t seqerr     : 1;  // 50
    uint64_t rsvd1      : 15; // 63-51    
    //
    uint64_t end_seq    : 36; // 35-0
    uint64_t api_patch  : 8;  // 43-36
    uint64_t api_minor  : 8;  // 51-44
    uint64_t api_major  : 8;  // 59-52    
    uint64_t rsvd2      : 4;  // 63-60        

  } MetaData;


  //
  // prototypes : encode/decode/dump
  //
  
  void           muedaq_encode_metadata        ( MetaData* ptr,
						 uint64_t run,
						 uint64_t start_seq, uint64_t end_seq,
						 uint64_t seqerr, uint64_t api_major,
						 uint64_t api_minor, uint64_t api_patch);
  void           muedaq_decode_metadata        ( MetaData* ptr,
						 uint64_t* run,
						 uint64_t* start_seq, uint64_t* end_seq,
						 uint64_t* seqerr, uint64_t* api_major,
						 uint64_t* api_minor, uint64_t* api_patch);
  uint64_t*      muedaq_metadata_to_raw        ( MetaData* metadata );
  MetaData*      muedaq_raw_to_metadata        ( uint64_t* raw );
  void           muedaq_print_metadata         ( MetaData* metadata );  
  
  
  //
  // prototypes : accessors
  //
  uint64_t muedaq_metadata_get_run(MetaData *);
  uint64_t muedaq_metadata_get_start_seq(MetaData *);
  uint64_t muedaq_metadata_get_end_seq(MetaData *);
  uint64_t muedaq_metadata_get_seqerr(MetaData *);
  uint64_t muedaq_metadata_get_api_major(MetaData *);
  uint64_t muedaq_metadata_get_api_minor(MetaData *);
  uint64_t muedaq_metadata_get_api_patch(MetaData *);
  void     muedaq_metadata_set_api_version(MetaData *, const char *);
  
#else

  typedef struct {
    uint64_t end_seq_1  : 16; // 15-0
    uint64_t start_seq  : 36; // 51-16
    uint64_t run        : 12; // 63-52      
    //
    uint64_t seqerr     : 1;  // 0
    uint64_t tstamp     : 43; // 43-1
    uint64_t end_seq_2  : 20; // 63-44
  } MetaData;
  

  //
  // prototypes : encode/decode/dump
  //
  
  void           muedaq_encode_metadata        ( MetaData* ptr,
						 uint64_t run,
						 uint64_t start_seq, uint64_t end_seq,
						 uint64_t seqerr, uint64_t tstamp );
  void           muedaq_decode_metadata        ( MetaData* ptr,
						 uint64_t* run,
						 uint64_t* start_seq, uint64_t* end_seq,
						 uint64_t* seqerr, uint64_t* tstamp );
  uint64_t*      muedaq_metadata_to_raw        ( MetaData* metadata );
  MetaData*      muedaq_raw_to_metadata        ( uint64_t* raw );
  void           muedaq_print_metadata         ( MetaData* metadata );  
  
  
  //
  // prototypes : accessors
  //
  uint64_t muedaq_metadata_get_run(MetaData *);
  uint64_t muedaq_metadata_get_start_seq(MetaData *);
  uint64_t muedaq_metadata_get_end_seq(MetaData *);
  uint64_t muedaq_metadata_get_tstamp(MetaData *);
  uint64_t muedaq_metadata_get_seqerr(MetaData *);



#endif
  


#ifdef __cplusplus
}
#endif  


#endif
