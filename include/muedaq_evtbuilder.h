#ifndef MUEDAQ_EVT_BUILDER
#define MUEDAQ_EVT_BUILDER

#include <ctype.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdbool.h>

#include "muedaq_file.h"
#include "muedaq_payload.h"

#define HEADER_CODE_POS       7
#define HEADER_CODE_BUILT     0xAB
#define HEADER_LENGTH_POS     6


// internal for the evtbuilder 
#define MUEDAQ_MAX_PAYLOAD_WORDS_JUMBO   150000000 // 1.18 GB / 8, rounded up
#define MUEDAQ_MAX_PAYLOAD_PACKETS_JUMBO 30000000  // 3M guess, refine later

// for offline use
#define MUEDAQ_MAX_PAYLOAD_WORDS_BUILT   2*MUEDAQ_MAX_PAYLOAD_WORDS // combination of 2 LinkFrames 
#define MUEDAQ_MAX_PAYLOAD_PACKETS_BUILT 1                          // only 1 packet per built frame 

#ifdef __cplusplus
extern "C" {
#endif

  
  
  //
  // definitions
  //

  // The BuilderHeader has an extended length field for use in the evtbuilder
  // It's also used in the frame data output from the evtbuilder, where it is
  // inserted between packets.  In that case length needs to be larger than
  // a LinkFrame, but should never be as larger as the internal BuilderFrame
  
  typedef struct {
    uint64_t length     : 36; // 35-0
    uint64_t run        : 12; // 47-36
    uint64_t rsvd       : 8;  // 55-48
    uint64_t code       : 8;  // 63-56          
  } BuilderHeader;


  // The JumboFrame should only be used in the evt builder
  
  typedef struct {
    
    pthread_mutex_t mtx;
    pthread_cond_t  cond;
    int             consumed;
    int             fillcount;
    int             frameID;
    
    BuilderHeader   header; // needs a header with extended length field
    
    PayloadPacket*  packet_ptr;
    PayloadPacket   packet[MUEDAQ_MAX_PAYLOAD_PACKETS_JUMBO];
    
    int             n_packets; 
    
    uint64_t*       buffer_ptr;  
    uint64_t        buffer[MUEDAQ_MAX_PAYLOAD_WORDS_JUMBO];
    
  } JumboFrame;


  // The BuilderFrame is output of the evtbuilder 
  
  typedef struct {
      
    BuilderHeader   header; // needs a jumbo header with extended length
    
    PayloadPacket*  packet_ptr;
    PayloadPacket   packet[MUEDAQ_MAX_PAYLOAD_PACKETS_BUILT];
    
    int             n_packets; 
    
    uint64_t*       buffer_ptr;  
    uint64_t        buffer[MUEDAQ_MAX_PAYLOAD_WORDS_BUILT];
    
  } BuilderFrame;  
  


  
  
  //
  // prototpyes : encode/decode/dump 
  //
  void           muedaq_encode_builder_header   (BuilderHeader*,unsigned,unsigned,unsigned);
  void           muedaq_decode_builder_header   (BuilderHeader*,unsigned*,unsigned*,unsigned*);
  uint64_t       muedaq_builder_header_to_raw   (BuilderHeader*);
  BuilderHeader* muedaq_raw_to_builder_header   (uint64_t*);
  bool           muedaq_next_builder_frame      (DAQFile*, BuilderFrame*);  
  void           muedaq_print_builder_header    (BuilderHeader*);

#ifdef EVB
  int            internal_compose_jumbo_frame   (DAQFile*, JumboFrame*, int, int);
#endif
  
  //  
  // prototype : accessors
  //
  
  uint64_t muedaq_builder_get_run(BuilderHeader *);
  uint64_t muedaq_builder_get_length(BuilderHeader *);
  uint64_t muedaq_builder_get_code(BuilderHeader *);
  
 
#ifdef __cplusplus
}
#endif


#endif
