#ifndef MUEDAQ_FILE
#define MUEDAQ_FILE

#include <stdio.h>
#include <ctype.h>
#include <arpa/inet.h>

#include "muedaq_metadata.h"

#define PACKET_DELIMITER    0xCDCDCDCDCDCDCDCDUL

#define MUEDAQ_FILE_GOOD 0
#define MUEDAQ_FILE_EOF  -1
#define MUEDAQ_FILE_BAD  -2

#ifdef __cplusplus
extern "C" {
#endif


  //
  // definitions
  //
 
  
  typedef struct {
    char         filename[1024];
    FILE         *fh;
    MetaData     metadata;
    int          was_read;
  } DAQFile;
  
  
  //
  // prototypes
  //
  
  void           muedaq_open_file              ( const char* filename, DAQFile * data );
  void           muedaq_close_file             ( DAQFile * data );
  
  
  
#ifdef __cplusplus
}
#endif

#endif
